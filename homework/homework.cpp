﻿// homework.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <vector>
#include <math.h>
#include <clocale>
#include <chrono>
#include <thread>
#include <string>

using namespace std;

double RandomExponentialDistribution(int exp_time) {
	
	double p, x;

	p = 0.01 * (rand() % 90) + 0.1;
	x = -exp_time * log(1 - p) * 60;

	return x;
}

enum WorkerState
{
	BUSY, FREE
};

enum MachineState
{
	IN_WORK, IN_REPAIR, IN_STAGNATION
};

class Shedule
{
public:
	int Start, End;
	WorkerState workerState;
	MachineState machineState;

	Shedule();

	Shedule(int start, int end, WorkerState state);
	Shedule(int start, int end, MachineState state);

	void setStart(int value) { Start = value; }
	void setEnd(int value) { End = value; }

	int getStart() const { return Start; }
	int getEnd() const { return End; }
	MachineState getStateMachine() const { return machineState; }
	WorkerState getStateWorker() const { return workerState; }

	~Shedule();
};

Shedule::Shedule(int start, int end, WorkerState state) :
	Start(start), 
	End(end),
	workerState(state)
{}

Shedule::Shedule(int start, int end, MachineState state) :
	Start(start),
	End(end),
	machineState(state)
{}

Shedule::Shedule() :
	Start(0),
	End(0)
{}


Shedule::~Shedule()
{}

class Workers
{
public:
	/// <summary>
	/// ЗП за час
	/// </summary>
	int costHour;

	/// <summary>
	/// Время конца действия
	/// </summary>
	int endTime;

	/// <summary>
	/// Расписание
	/// </summary>
	vector<Shedule*> shedule;

	/// <summary>
	/// Проверяем, входит ли данное время (time) в промежуток занятости рабочего
	/// </summary>
	/// <param name="time"></param>
	/// <returns></returns>
	WorkerState GetInfo(int time);
	
	Workers()
	{

	}

	void AddIntervalTime(int begin, int end, WorkerState state);

	Workers(int cost);
	~Workers();
};

WorkerState Workers::GetInfo(int time)
{
	for (int i = 0; i < shedule.size(); i++) {
		if (shedule[i]->Start < time && shedule[i]->End > time)
			return BUSY;
	}
	return FREE;
}

void Workers::AddIntervalTime(int begin, int end, WorkerState state)
{
	shedule.push_back(new Shedule(begin, end, state));
	endTime = end;
}

Workers::Workers(int cost) : costHour(cost)
{
}

Workers::~Workers()
{
}

class Machine
{
public:
	/// <summary>
	/// Расход в час во время застоя
	/// </summary>
	int decreaseHour;
	
	/// <summary>
	/// Доход в час во время работы
	/// </summary>
	int profitHour;
	
	/// <summary>
	/// Время конца действия в мин
	/// </summary>
	int endTime;

	/// <summary>
	/// Расписание
	/// </summary>
	vector<Shedule*> shedule;

	// Математическое ожидание времени работы/ремонта
	int expectedTime;

	Machine()
	{

	}

	void AddIntervalTime(int begin, int end, MachineState state);

	Machine(int profit, int decrease, int expTime);
	~Machine();
};

Machine::Machine(int profit, int decrease, int expTime) :
	profitHour(profit),
	decreaseHour(decreaseHour),
	expectedTime(expTime),
	endTime(0)
{}

void Machine::AddIntervalTime(int begin, int end, MachineState state)
{
	shedule.push_back(new Shedule(begin, end, state));
	endTime = end;
}

Machine::~Machine()
{}

class DataBase
{

public:
	Workers *slesar6;
	Workers *slesar3;
	Machine *buldozer;
	Machine *excavator;

	DataBase()
	{

	}

	DataBase(vector<Workers*> workers, Machine *b, Machine *e) {
		buldozer = b;
		excavator = e;

		//workers[0] - первый элемент списка
		slesar6 = workers[0];

		// Если в работе участвуют 2 слесаря
		if (workers.size() == 2)
			slesar3 = workers[1];
	}
	~DataBase() {}
};


/// <summary>
/// 
/// </summary>
/// <param name="m1">Машина, которая раньше закончила работать</param>
/// <param name="m2">Машина, которая позже закончила работать</param>
/// <param name="exp_time_m1_6"></param>
/// <param name="exp_time_m2_6"></param>
/// <param name="sl6"></param>
void setRepair(Machine* m1, Machine* m2, int exp_time_m1_6, int exp_time_m2_6, vector<Workers*> slesars) {

	int startTime = m1->endTime + 1;
	int continueTime = RandomExponentialDistribution(exp_time_m1_6);

	m1->AddIntervalTime(startTime, startTime + continueTime, IN_REPAIR);

	for (Workers* slesar : slesars)
		slesar->AddIntervalTime(startTime, startTime + continueTime, BUSY);

	if (slesars[0]->GetInfo(m2->endTime + 1) == BUSY) {
		m2->AddIntervalTime(m2->endTime, m1->endTime, IN_STAGNATION);

		startTime = m2->endTime + 1;
		continueTime = RandomExponentialDistribution(exp_time_m2_6);
		m2->AddIntervalTime(startTime, startTime + continueTime, IN_REPAIR);
		for (Workers* slesar : slesars)
			slesar->AddIntervalTime(startTime, startTime + continueTime, BUSY);
	}
}

void normTime(Machine* machine, int time) {

	for (int i = 0; i < machine->shedule.size(); i++)
		if (machine->shedule[i]->getStart() <= time * 60 && machine->shedule[i]->getEnd() >= time * 60)
		{
			machine->shedule[i]->setEnd(time * 60);
			machine->shedule.erase(machine->shedule.begin() + i + 1, machine->shedule.end());
			break;
		}

}

void normTime(Workers* slesar, int time) {

	for (int i = 0; i < slesar->shedule.size(); i++)
		if (slesar->shedule[i]->getStart() >= time * 60 && slesar->shedule[i]->getEnd() >= time * 60)
		{
			slesar->shedule.erase(slesar->shedule.begin() + i + 1, slesar->shedule.end()) - 1;
			break;
		}
		else if (slesar->shedule[i]->getStart() <= time * 60 && slesar->shedule[i]->getEnd() >= time * 60)
		{
			slesar->shedule[i]->setEnd(time * 60);
			slesar->shedule.erase(slesar->shedule.begin() + i + 1, slesar->shedule.end());
			break;
		}

}


int day = 1;
double allSum = 0;
double wTime = 0;
double wbill = 0;

void printGraphic(vector<Machine*> machines, vector<Workers*> slesars) {

	double sum = 0;

	cout << "\nОтчет за " << day << " день: " << endl;
	
	string workInt = "Работал: \n";
	string repairInt = "Ремонтировался: \n";
	string stagnInt = "Находился в застое: \n";

	for (auto sh : machines[0]->shedule)
	{
		if (sh->getStateMachine() == IN_WORK)
			workInt += to_string(sh->getStart() / 60) + ":" + to_string(sh->getStart() % 60) + " - " + to_string(sh->getEnd() / 60) + ":" + to_string(sh->getEnd() % 60) + "\n";
		if (sh->getStateMachine() == IN_REPAIR)
			repairInt += to_string(sh->getStart() / 60) + ":" + to_string(sh->getStart() % 60) + " - " + to_string(sh->getEnd() / 60) + ":" + to_string(sh->getEnd() % 60) + "\n";
		if (sh->getStateMachine() == IN_STAGNATION)
			stagnInt += to_string(sh->getStart() / 60) + ":" + to_string(sh->getStart() % 60) + " - " + to_string(sh->getEnd() / 60) + ":" + to_string(sh->getEnd() % 60) + "\n";
	}

	cout << "График экскаватора: " << endl;
	cout << workInt << endl;
	cout << repairInt << endl;
	cout << stagnInt << endl;

	workInt = "Работал: \n";
	repairInt = "Ремонтировался: \n";
	stagnInt = "Находился в застое: \n";

	for (auto sh : machines[1]->shedule) 
	{
		if (sh->getStateMachine() == IN_WORK)
			workInt += to_string(sh->getStart() / 60) + ":" + to_string(sh->getStart() % 60) + " - " + to_string(sh->getEnd() / 60) + ":" + to_string(sh->getEnd() % 60 ) + "\n";
		if (sh->getStateMachine() == IN_REPAIR)
			repairInt += to_string(sh->getStart() / 60) + ":" + to_string(sh->getStart() % 60) + " - " + to_string(sh->getEnd() / 60) + ":" + to_string(sh->getEnd() % 60 ) + "\n";
		if (sh->getStateMachine() == IN_STAGNATION)
			stagnInt += to_string(sh->getStart() / 60) + ":" + to_string(sh->getStart() % 60) + " - " + to_string(sh->getEnd() / 60) + ":" + to_string(sh->getEnd() % 60) + "\n";
	}

	cout << "График бульдозера: " << endl;

	cout << workInt << endl;
	cout << repairInt << endl;
	cout << stagnInt << endl;

	for (Machine* machine : machines)
	{
		int k = 0;
		for (auto sh : machine->shedule) {
			if (k != machine->shedule.size() - 1)
				sum = (sh->getStateMachine() == IN_WORK) ? sum + (sh->getEnd() - sh->getStart()) * machine->profitHour / 60 : sum - (sh->getEnd() - sh->getStart()) * machine->decreaseHour / 60;
			k++;
		}
	}

	double machineSum = sum;

	workInt = "Работал: \n";

	for (auto sh : slesars[0]->shedule)
	{
		if (sh->getStateWorker() == BUSY)
			workInt += to_string(sh->getStart() / 60) + ":" + to_string(sh->getStart() % 60) + " - " + to_string(sh->getEnd() / 60) + ":" + to_string(sh->getEnd() % 60) + "\n";
	}

	cout << "Слесарь 6-го разряда: " << endl;
	cout << workInt << endl;

	workInt = "Работал: \n";

	if (slesars.size() > 1)
	{
		cout << "Слесарь 3-го разряда: " << endl;

		for (auto sh : slesars[1]->shedule)
		{
			if (sh->getStateWorker() == BUSY)
				workInt += to_string(sh->getStart() / 60) + ":" + to_string(sh->getStart() % 60) + " - " + to_string(sh->getEnd() / 60) + ":" + to_string(sh->getEnd() % 60) + "\n";
		}
		cout << workInt << endl;
	}

	cout << "Доход от машин: " << machineSum << endl;

	for (Workers* slesar : slesars)
	{
		int k = 0; 
		for (auto sh : slesar->shedule) {
			if (k != slesar->shedule.size() - 1)
				sum -= (sh->getEnd() - sh->getStart()) * slesar->costHour / 60;
		}
	}

	sum -= wTime * wbill;

	cout << "Чистый доход: " << sum << endl;
	day++;
	allSum += sum;

}

void CreateOneShedule(
	int m_ex,
	int m_bul,
	double m6_ex,
	double m6_bul,
	int decreaseEx,
	int decreaseBul,
	int profitEx,
	int profitBul,
	int salary6,
	int waybill,
	int daysCount,
	int workingTime,
	int profilacticTime,
	double m63_ex = 0,
	double m63_bul = 0,
	int salary3 = 0
)
{
	Machine* exc = new Machine(profitEx, decreaseEx, m_ex);
	Machine* buld = new Machine(profitBul, decreaseBul, m_bul);
	Workers* slesar6 = new Workers(salary6);
	Workers* slesar3 = nullptr;

	if (salary3 != 0) {
		slesar3 = new Workers(salary3);
	}

	if (!slesar3)
	{
		while (exc->endTime <= workingTime * 60 && buld->endTime <= workingTime * 60) {
			double timeWork = RandomExponentialDistribution(buld->expectedTime);
			buld->AddIntervalTime(buld->endTime, buld->endTime + timeWork, IN_WORK);

			timeWork = RandomExponentialDistribution(exc->expectedTime);
			exc->AddIntervalTime(exc->endTime, exc->endTime + timeWork, IN_WORK);

			if (exc->endTime <= buld->endTime) {
				setRepair(exc, buld, m6_ex, m6_bul, { slesar6 });
			}
			else
				setRepair(buld, exc, m6_bul, m6_ex, { slesar6 });
		}
	}
	else
	{
		while (exc->endTime <= workingTime * 60 && buld->endTime <= workingTime * 60) {
			double timeWork = RandomExponentialDistribution(buld->expectedTime);
			buld->AddIntervalTime(buld->endTime, buld->endTime + timeWork, IN_WORK);

			timeWork = RandomExponentialDistribution(exc->expectedTime);
			exc->AddIntervalTime(exc->endTime, exc->endTime + timeWork, IN_WORK);

			if (exc->endTime <= buld->endTime) {
				setRepair(exc, buld, m63_ex, m63_bul, { slesar6, slesar3 });
			}
			else
				setRepair(buld, exc, m63_bul, m63_ex, { slesar6, slesar3 });
		}
	}

	normTime(buld, workingTime);
	normTime(exc, workingTime);

	exc->AddIntervalTime(workingTime * 60 + 1, (workingTime + profilacticTime) * 60, IN_REPAIR);
	buld->AddIntervalTime(workingTime * 60 + 1, (workingTime + profilacticTime) * 60, IN_REPAIR);

	if (slesar3)
		normTime(slesar3, workingTime);
	
	normTime(slesar6, workingTime);

	vector<Workers*> one = { slesar6 };
	vector<Workers*> two = { slesar6, slesar3 };

	printGraphic({ exc, buld }, slesar3 ? two : one);

}

void CreateFullShedule(
	int m_ex,
	int m_bul,
	double m6_ex,
	double m6_bul,
	int decreaseEx,
	int decreaseBul,
	int profitEx,
	int profitBul,
	int salary6,
	int waybill,
	int daysCount,
	int workingTime,
	int profilacticTime,
	double m63_ex = 0,
	double m63_bul = 0,
	int salary3 = 0
)
{
	for (int i = 0; i < daysCount; i++) {
		
		if (salary3 == 0)
			CreateOneShedule(m_ex,
				m_bul,
				m6_ex,
				m6_bul,
				decreaseEx,
				decreaseBul,
				profitEx,
				profitBul,
				salary6,
				waybill,
				daysCount,
				workingTime,
				profilacticTime);
		else
		{
			CreateOneShedule(m_ex,
				m_bul,
				m6_ex,
				m6_bul,
				decreaseEx,
				decreaseBul,
				profitEx,
				profitBul,
				salary6,
				waybill,
				daysCount,
				workingTime,
				profilacticTime,
				m63_ex,
				m63_bul,
				salary3);
		}
	}

	cout << "\nОбщий чистый доход: " << allSum;

}

int main()
{
	srand(time(NULL));
	setlocale(LC_ALL, "RUS");
	cout << "Введите математическое ожидание продолжительности работ для экскаватора: ";
	int m_ex; cin >> m_ex;
	
	cout << "Введите математическое ожидание продолжительности работ для бульдозера: ";	
	int m_bul; cin >> m_bul;

	int slesar_count;

	while (1) {
		cout << "Сколько в бригаде слесарей? ";
		cin >> slesar_count;

		if (slesar_count > 2 || slesar_count < 1)
			cout << "Неверный ввод данных." << endl;
		else
			break;
	}
	
	bool one_slesar = slesar_count == 1;

	double m6_ex, m6_bul, m63_ex, m63_bul;

	if (one_slesar) {
		cout << "Введите математическое ожидание продолжительности ремонта слесарем 6 разряда для экскаватора: ";
		cin >> m6_ex;

		cout << "Введите математическое ожидание продолжительности ремонта слесарем 6 разряда для бульдозера: ";
		cin >> m6_bul;
	}
	else {
		cout << "Введите математическое ожидание продолжительности ремонта двумя слесарями для экскаватора: " ;
		cin >> m63_ex;

		cout << "Введите математическое ожидание продолжительности ремонта двумя слесарями для бульдозера: ";
		cin >> m63_bul;
	}

	int decreaseEx, decreaseBul;

	cout << "Введите убытки во время застоя экскаватора (руб в час): ";
	cin >> decreaseEx;
	
	cout << "Введите убытки во время застоя бульдозера (руб в час): ";
	cin >> decreaseBul;

	int profitEx, profitBul;
	cout << "Введите доход во время работы экскаватора (руб в час): ";
	cin >> profitEx;

	cout << "Введите доход во время работы бульдозера (руб в час): ";
	cin >> profitBul;

	int salary6, salary3;

	if (!one_slesar) {
		cout << "Введите зарплату слесаря 3-го разряда (руб в час): ";
		cin >> salary3;
	}

	cout << "Введите зарплату слесаря 6-го разряда (руб в час): ";
	cin >> salary6;

	int waybill;
	cout << "Введите накладные расходы за бригаду (руб в час): ";
	cin >> waybill;
	wbill = waybill;

	int daysCount;
	cout << "Введите количество дней имитационного процесса: ";
	cin >> daysCount;

	int workingTime, profilacticTime;
	cout << "Введите количество часов работы машин: ";
	cin >> workingTime;
	wTime = workingTime;

	cout << "Введите количество часов профилактики машин: ";
	cin >> profilacticTime;

	if (one_slesar)
		CreateFullShedule(m_ex,
			m_bul,
			m6_ex,
			m6_bul,
			decreaseEx,
			decreaseBul,
			profitEx,
			profitBul,
			salary6,
			waybill,
			daysCount,
			workingTime,
			profilacticTime);
	else 
		CreateFullShedule(m_ex,
			m_bul,
			0,
			0,
			decreaseEx,
			decreaseBul,
			profitEx,
			profitBul,
			salary6,
			waybill,
			daysCount,
			workingTime,
			profilacticTime,
			m63_ex,
			m63_bul, 
			salary3);

	return 0;
}



